package com.humanresources.permissao;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "Permissão", description = "CRUD de Permissão")
@RestController
@RequestMapping("/permissoes")
public class PermissaoController {

    @Autowired
    private PermissaoService permissaoService;

    @GetMapping
    public ResponseEntity listar(Pageable pageable) {
        return ResponseEntity.ok(permissaoService.listar(pageable));
    }

    @GetMapping("/{idPermissao}")
    public ResponseEntity buscar(@PathVariable Long idPermissao) {
        return ResponseEntity.ok(permissaoService.buscarOuFalhar(idPermissao));
    }

    @PostMapping
    public ResponseEntity salvar(//UriComponentsBuilder uriComponentsBuilder,
                                 @RequestBody PermissaoForm permissaoForm) {

        return ResponseEntity.ok(permissaoService.salvar(permissaoForm));
    }

    @PutMapping("/{idPermissao}")
    public ResponseEntity atualizar(@PathVariable Long idPermissao,
                                    @RequestBody PermissaoForm permissaoForm) {

        return ResponseEntity.ok(permissaoService.atualizar(idPermissao, permissaoForm));
    }

    @DeleteMapping("/{idPermissao}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long idPermissao) {

        permissaoService.remover(idPermissao);
    }
}
