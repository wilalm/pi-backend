package com.humanresources.avaliacao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface AvaliacaoRepository extends JpaRepository<Avaliacao, Long> {

    Page<Avaliacao> findAllByColaboradorIdColaborador(Long idColaborador, Pageable pageable);
    Optional<Avaliacao> findByColaboradorIdColaboradorAndIdAvaliacao(Long idColaborador, Long idAvaliacao);
    Optional<Avaliacao> findAllByGestorIdColaborador(Long idGestor);


    //relatorios
    List<Avaliacao> findByDataBetween(LocalDate dataInicio, LocalDate dataFim);
}
