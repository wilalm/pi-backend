package com.humanresources.evento;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.humanresources.colaborador.Colaborador;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Evento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEvento;

    @Enumerated(EnumType.STRING)
    private TipoEvento tipoEvento;

    private String descricao;
    private LocalDateTime data;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_colaborador")
    private Colaborador colaborador;

}
