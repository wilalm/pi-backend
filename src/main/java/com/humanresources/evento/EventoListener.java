package com.humanresources.evento;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class EventoListener {

    @Autowired
    private EventoService eventoService;

    @Async
    @EventListener
    public void handleEvent(Event evento) {
        System.out.println("eventou");
        eventoService.save(evento.toEvento());
    }
}
