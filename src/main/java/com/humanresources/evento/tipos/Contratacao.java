package com.humanresources.evento.tipos;

import com.humanresources.colaborador.Colaborador;
import com.humanresources.evento.Event;
import com.humanresources.evento.Evento;
import com.humanresources.evento.TipoEvento;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class Contratacao implements Event {

    private static final TipoEvento evento = TipoEvento.CONTRATACAO;

    private LocalDateTime dataContratacao;
    private List<Colaborador> colaboradores;

    public List<Evento> toEvento() {
        return colaboradores.stream()
                .map(colaborador ->
                        Evento.builder()
                                .tipoEvento(evento)
                                .data(dataContratacao)
                                .colaborador(colaborador)
                                .descricao(evento.getMensagem())
                                .build())
                .collect(Collectors.toList());
    }
}
