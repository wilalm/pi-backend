package com.humanresources.evento.tipos;

import com.humanresources.colaborador.Colaborador;
import com.humanresources.evento.Event;
import com.humanresources.evento.Evento;
import com.humanresources.evento.TipoEvento;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class AlteracaoSalario implements Event {

    private static final TipoEvento evento = TipoEvento.ALTERACAO_SALARIO;
    private static final LocalDateTime data = LocalDateTime.now();

    private BigDecimal novoSalario;
    private List<Colaborador> colaboradores;

    public List<Evento> toEvento() {
        return colaboradores.stream()
                .map(colaborador ->
                        Evento.builder()
                                .tipoEvento(evento)
                                .data(data)
                                .colaborador(colaborador)
                                .descricao(
                                        String.format(
                                                evento.getMensagem(),
                                                colaborador.getSalario(),
                                                novoSalario))
                                .build())
                .collect(Collectors.toList());
    }
}
