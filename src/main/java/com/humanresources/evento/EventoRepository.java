package com.humanresources.evento;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface EventoRepository extends JpaRepository<Evento, Long> {

    List<Evento> findAllByDataBetweenAndColaboradorIdColaboradorOrderByDataDesc(LocalDateTime dataInicio, LocalDateTime dataFim, Long idColaborador);
}
