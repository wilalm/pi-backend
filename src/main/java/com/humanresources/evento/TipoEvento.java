package com.humanresources.evento;

import lombok.Getter;

@Getter
public enum TipoEvento {

    CONTRATACAO("Contratação efetuada"),
    ALTERACAO_SALARIO("Alteração no salário de R$%s para R$%s"),
    ALTERACAO_CARGO("Alteração no cargo, movido de %s para %s com promoção salarial de R$%s para R$%s.\nBenefícios perdidos: %s\nBenefícios ganhados: %s"),
    ALTERACAO_DEPARTAMENTO("Alteração no departamento, movido de %s para %s"),
    INCLUSAO_AVALIACAO("Inclusão de nova avaliação"),
    INCLUSAO_BENEFICIO("Inclusão do benefício %s no valor de %s. Coparticipação: %s%%"),
    ALTERACAO_BENEFICIO("Alteração no benefício %s, valor alterado de R$%.2f para R$%.2f, coparticipação alterada de %d%% para %d%%"),
    REMOCAO_BENEFICIO("Remoção do benefício %s");

    private String mensagem;

    TipoEvento(String mensagem) {
        this.mensagem = mensagem;
    }
}
