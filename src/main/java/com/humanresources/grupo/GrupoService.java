package com.humanresources.grupo;

import com.humanresources.exception.exceptions.EntidadeEmUsoException;
import com.humanresources.exception.exceptions.EntidadeNaoEncontradaException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class GrupoService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private GrupoRepository grupoRepository;

    public Page<Grupo> listar(Pageable pageable) {
        return grupoRepository.findAll(pageable);
    }

    public Grupo buscarOuFalhar(Long idGrupo) {
        return grupoRepository.findById(idGrupo)
                .orElseThrow(() ->
                        new EntidadeNaoEncontradaException(
                                String.format("Não existe um grupo com id %s", idGrupo)));
    }


    public Grupo salvar(GrupoForm grupoForm) {

        Grupo novoGrupo = grupoForm.transformar();

        return grupoRepository.save(novoGrupo);
    }


    public Grupo atualizar(Long idGrupo, GrupoForm grupoForm) {
        Grupo beneficioAtual = buscarOuFalhar(idGrupo);

        modelMapper.map(grupoForm, beneficioAtual);

        return grupoRepository.save(beneficioAtual);
    }

    public void remover(Long idGrupo) {

        try {
            Grupo grupo = buscarOuFalhar(idGrupo);
            grupoRepository.delete(grupo);
        } catch (DataIntegrityViolationException e) {
            throw new EntidadeEmUsoException(String.format("O grupo %s não pode ser excluído pois possui usuarios que o utilizam", idGrupo));
        }
    }
}
