package com.humanresources.cargo;

import com.humanresources.exception.exceptions.EntidadeEmUsoException;
import com.humanresources.exception.exceptions.EntidadeNaoEncontradaException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CargoService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private CargoRepository cargoRepository;

    public List<Cargo> listar() {
        return cargoRepository.findAll();
    }

    public Page<Cargo> listar(Pageable pageable) {
        return cargoRepository.findAll(pageable);
    }

    public Cargo buscarOuFalhar(Long idCargo) {
        return cargoRepository.findById(idCargo)
                .orElseThrow(() ->
                        new EntidadeNaoEncontradaException(
                                String.format("Não existe um cargo com id %s", idCargo)));
    }


    public Cargo salvar(CargoForm cargoForm) {

        Cargo novoCargo = cargoForm.transformar();

        return cargoRepository.save(novoCargo);
    }


    public Cargo atualizar(Long idCargo, CargoForm cargoForm) {
        Cargo cargoAtual = buscarOuFalhar(idCargo);

        modelMapper.map(cargoForm, cargoAtual);

        return cargoRepository.save(cargoAtual);
    }

    public void remover(Long idCargo) {

        try {
            Cargo cargo = buscarOuFalhar(idCargo);

            cargoRepository.delete(cargo);
        } catch (DataIntegrityViolationException e) {
            throw new EntidadeEmUsoException(String.format("O cargo %s não pode ser excluído pois possui funcionários vinculados à ele", idCargo));
        }
    }

    public List<Cargo> listaCargosQuePossuemOBeneficio(Long idBeneficio) {
        return cargoRepository.findAllByBeneficios_IdBeneficio(idBeneficio);
    }
}
