package com.humanresources.cargo;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "Cargo", description = "CRUD de Cargo")
@RestController
@RequestMapping("/cargos")
public class CargoController {

    @Autowired
    private CargoService cargoService;

    @GetMapping
    public ResponseEntity listar(Pageable pageable) {
        return ResponseEntity.ok(cargoService.listar(pageable));
    }

    @GetMapping("/{idCargo}")
    public ResponseEntity buscar(@PathVariable Long idCargo) {
        return ResponseEntity.ok(cargoService.buscarOuFalhar(idCargo));
    }

    @PostMapping
    public ResponseEntity salvar(//UriComponentsBuilder uriComponentsBuilder,
                                 @RequestBody CargoForm cargoForm) {

        return ResponseEntity.ok(cargoService.salvar(cargoForm));
    }

    @PutMapping("/{idCargo}")
    public ResponseEntity atualizar(@PathVariable Long idCargo,
                                    @RequestBody CargoForm cargoForm) {

        return ResponseEntity.ok(cargoService.atualizar(idCargo, cargoForm));
    }

    @DeleteMapping("/{idCargo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long idCargo) {

        cargoService.remover(idCargo);
    }
}
