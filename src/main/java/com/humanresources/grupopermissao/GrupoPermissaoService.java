package com.humanresources.grupopermissao;

import com.humanresources.grupo.Grupo;
import com.humanresources.grupo.GrupoService;
import com.humanresources.permissao.Permissao;
import com.humanresources.permissao.PermissaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

@Service
public class GrupoPermissaoService {

    @Autowired
    private GrupoService grupoService;

    @Autowired
    private PermissaoService permissaoService;

    public Set<Permissao> listar(Long idCargo) {
        Grupo grupo = grupoService.buscarOuFalhar(idCargo);

        return grupo.getPermissoes();
    }

    @Transactional
    public void associar(Long idCargo, Long idPermissao) {
        Grupo grupo = grupoService.buscarOuFalhar(idCargo);

        Permissao permissao = permissaoService.buscarOuFalhar(idPermissao);

        grupo.adicionarPermissao(permissao);
    }

    @Transactional
    public void desassociar(Long idGrupo, Long idPermissao) {
        Grupo grupo = grupoService.buscarOuFalhar(idGrupo);

        Permissao permissao = permissaoService.buscarOuFalhar(idPermissao);

        grupo.removerPermissao(permissao);
    }
}
