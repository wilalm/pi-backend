package com.humanresources.grupopermissao;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "GrupoPermissão", description = "Associação entre Grupo e Permissão")
@RestController
@RequestMapping("/grupos/{idGrupo}/permissoes")
public class GrupoPermissaoController {

    @Autowired
    private GrupoPermissaoService grupoPermissaoService;

    @GetMapping
    public ResponseEntity listar(@PathVariable Long idGrupo) {
        return ResponseEntity.ok(grupoPermissaoService.listar(idGrupo));
    }

    @PutMapping("/{idPermissao}")
    public ResponseEntity associar(@PathVariable Long idGrupo,
                                   @PathVariable Long idPermissao) {
        grupoPermissaoService.associar(idGrupo, idPermissao);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{idPermissao}")
    public ResponseEntity desassociar(@PathVariable Long idGrupo,
                                       @PathVariable Long idPermissao) {
        grupoPermissaoService.desassociar(idGrupo, idPermissao);

        return ResponseEntity.ok().build();
    }
}
