package com.humanresources.promocao.coletiva;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class PromocaoColetivaRequest {

    private List<Long> idColaboradores;
    private BigDecimal novoSalario;

    @JsonProperty("novo_cargo")
    private Long idCargo;
}
