package com.humanresources.promocao.individual;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.humanresources.cargo.Cargo;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Set;

@Data
@Builder
public class PromocaoIndividualResponse {

    private BigDecimal salarioMaximoRecomendadoParaOCargoAtual;

    @JsonIgnoreProperties({
            "beneficios"
    })
    private Set<Cargo> cargosIndicados;

    @JsonIgnoreProperties({
            "beneficios"
    })
    private Set<Cargo> cargosPossiveis;
}
