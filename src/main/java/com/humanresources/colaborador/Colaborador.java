package com.humanresources.colaborador;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.humanresources.avaliacao.Avaliacao;
import com.humanresources.cargo.Cargo;
import com.humanresources.departamento.Departamento;
import com.humanresources.evento.Evento;
import com.humanresources.usuario.Usuario;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Colaborador {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idColaborador;

    private String nome;
    private String rg;
    private String cpf;
    private String ctps;
    private LocalDate dataNascimento;
    private BigDecimal salario;
    private LocalDate dataContratacao;

    @JsonIgnoreProperties({
            "descricao", "localizacao", "colaboradores"
    })
    @ManyToOne
    @JoinColumn(name = "id_departamento")
    private Departamento departamento;

    @JsonIgnoreProperties({
            "descricao", "salario_min_recomendado", "salario_max_recomendado", "beneficios"
    })
    @ManyToOne
    @JoinColumn(name = "id_cargo")
    private Cargo cargo;

    @ManyToOne
    @JsonIgnoreProperties({"gestor", "data_contratacao", "rg", "cpf", "ctps", "data_nascimento", "salario", "departamento", "cargo", "avaliacoes", "avaliacoes_feitas", "usuario", "eventos"})
    @JoinColumn(name = "id_gestor")
    private Colaborador gestor;

    @OneToMany(mappedBy = "colaborador", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Avaliacao> avaliacoes;

    @OneToMany(mappedBy = "gestor", cascade = CascadeType.ALL)
    private List<Avaliacao> avaliacoesFeitas;

    @OneToMany(mappedBy = "colaborador", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties({
            "colaborador"
    })
    private List<Usuario> usuario;

//    public Usuario getUsuario() {
//        if (!usuario.isEmpty()) {
//            return usuario.get(0);
//        }
//        return null;
//    }

    public Integer getIdade() {
        LocalDate hoje = LocalDate.now();
        int idade = hoje.getYear() - dataNascimento.getYear();

        if (hoje.getDayOfYear() < dataNascimento.getDayOfYear()) {
            idade--;
        }

        return idade;
    }

    @OneToMany(mappedBy = "colaborador", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Evento> eventos;

    public void adicionarAvaliacao(Avaliacao avaliacao) {
        getAvaliacoes().add(avaliacao);
    }

    public void removerAvaliacao(Avaliacao avaliacao) {
        getAvaliacoes().remove(avaliacao);
    }
}
