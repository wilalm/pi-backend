package com.humanresources.colaborador;

import com.humanresources.avaliacao.AvaliacaoService;
import com.humanresources.cargo.Cargo;
import com.humanresources.cargo.CargoService;
import com.humanresources.departamento.Departamento;
import com.humanresources.departamento.DepartamentoService;
import com.humanresources.evento.tipos.AlteracaoCargo;
import com.humanresources.evento.tipos.AlteracaoDepartamento;
import com.humanresources.evento.tipos.AlteracaoSalario;
import com.humanresources.evento.tipos.Contratacao;
import com.humanresources.exception.exceptions.EntidadeEmUsoException;
import com.humanresources.exception.exceptions.EntidadeNaoEncontradaException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
public class ColaboradorService {

    @PersistenceContext
    private EntityManager manager;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ColaboradorRepository colaboradorRepository;

    @Autowired
    private CargoService cargoService;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private AvaliacaoService avaliacaoService;

    @Autowired
    private DepartamentoService departamentoService;

    public Page<Colaborador> listar(Pageable pageable) {
        return colaboradorRepository.findAll(pageable);
    }

    public List<Colaborador> listar(List<Long> idColaboradores) {
        return colaboradorRepository.findAllByIdColaboradorIn(idColaboradores);
    }

    public List<Colaborador> listar() {
        return colaboradorRepository.findAll();
    }

    public List<Colaborador> listarGerenciadosPor(Long idGestor) {
        buscarOuFalhar(idGestor);
        return colaboradorRepository.findAllByGestor_IdColaborador(idGestor);
    }

    public Colaborador buscarOuFalhar(Long idColaborador) {
        return colaboradorRepository.findById(idColaborador)
                .orElseThrow(() ->
                        new EntidadeNaoEncontradaException(
                                String.format("Não existe um colaborador com id %s", idColaborador)));
    }


    public Colaborador salvar(ColaboradorForm colaboradorForm) {

        Cargo cargo = cargoService.buscarOuFalhar(colaboradorForm.getIdCargo());
        Departamento departamento = departamentoService.buscarOuFalhar(colaboradorForm.getIdDepartamento());

        if (Objects.nonNull(colaboradorForm.getIdGestor())) {
            colaboradorForm.setGestor(buscarOuFalhar(colaboradorForm.getIdGestor()));
        }

        Colaborador colaborador = colaboradorForm.transformar(cargo, departamento);
        Colaborador novoColaborador = colaboradorRepository.save(colaborador);

        publisher.publishEvent(new Contratacao(colaboradorForm.getDataContratacao().atTime(9, 0, 0), Collections.singletonList(novoColaborador)));
        return novoColaborador;
    }

    @Transactional
    public Colaborador atualizar(Long idColaborador, ColaboradorForm colaboradorForm) {
        Colaborador colaboradorAtual = buscarOuFalhar(idColaborador);

        if (colaboradorForm.getIdCargo() != null) {
            Cargo cargoAntino = colaboradorAtual.getCargo();
            Cargo novoCargo = cargoService.buscarOuFalhar(colaboradorForm.getIdCargo());
            publisher.publishEvent(new AlteracaoCargo(cargoAntino, novoCargo, colaboradorForm.getSalario(), Collections.singletonList(colaboradorAtual)));
            colaboradorForm.setCargo(novoCargo);
        } else if (colaboradorForm.getSalario() != null) {
            publisher.publishEvent(new AlteracaoSalario(colaboradorForm.getSalario(), Collections.singletonList(colaboradorAtual)));
        }

        if (colaboradorForm.getIdDepartamento() != null) {
            Departamento novoDepartamento = departamentoService.buscarOuFalhar(colaboradorForm.getIdDepartamento());
            publisher.publishEvent(new AlteracaoDepartamento(novoDepartamento, Collections.singletonList(colaboradorAtual)));
            colaboradorForm.setDepartamento(novoDepartamento);
        }

        if (colaboradorForm.getIdGestor() != null) {
            Colaborador gestor = buscarOuFalhar(colaboradorForm.getIdGestor());
            colaboradorForm.setGestor(gestor);
        }

        modelMapper.map(colaboradorForm, colaboradorAtual);

        return colaboradorRepository.save(colaboradorAtual);
    }

    @Transactional
    public void promoverPorCargo(List<Colaborador> colaboradores, Cargo novoCargo, BigDecimal novoSalario) {
        List<Colaborador> colaboradoresParaEvento = new ArrayList<>(colaboradores);
        Cargo cargoAntivo = colaboradores.get(0).getCargo();
        publisher.publishEvent(new AlteracaoCargo(cargoAntivo, novoCargo, novoSalario, colaboradoresParaEvento));
        colaboradores.forEach(colaborador -> {
            colaborador.setCargo(novoCargo);
            colaborador.setSalario(novoSalario);
        });
    }

    @Transactional
    public void promoverPorSalario(List<Colaborador> colaboradores, BigDecimal novoSalario) {
        publisher.publishEvent(new AlteracaoSalario(novoSalario, colaboradores));
        colaboradores.forEach(colaborador -> colaborador.setSalario(novoSalario));
    }

    @Transactional
    public void promoverPorCargo(Colaborador colaborador, Cargo novoCargo, BigDecimal novoSalario) {
        List<Colaborador> colaboradores = new ArrayList<>(Collections.singletonList(colaborador));
        Cargo cargoAntino = colaborador.getCargo();
        publisher.publishEvent(new AlteracaoCargo(cargoAntino, novoCargo, novoSalario, colaboradores));
        colaborador.setCargo(novoCargo);
        colaborador.setSalario(novoSalario);
    }

    @Transactional
    public void promoverPorSalario(Colaborador colaborador, BigDecimal novoSalario) {
        List<Colaborador> colaboradores = new ArrayList<>(Collections.singletonList(colaborador));
        publisher.publishEvent(new AlteracaoSalario(novoSalario, colaboradores));
        colaborador.setSalario(novoSalario);
    }


    //TODO
    public void remover(Long idColaborador) {
        Colaborador colaborador = buscarOuFalhar(idColaborador);
        boolean possuiAvaliacaoComoGestor = avaliacaoService.possuiAvaliacaoComoGestor(colaborador);

        if (Objects.isNull(colaborador.getGestor())) {
            throw new EntidadeEmUsoException(String.format("O colaborador %s não pode ser exclúido pois ele é gestor de outros colaboradores", colaborador.getIdColaborador()));
        }

        if (possuiAvaliacaoComoGestor) {
            throw new EntidadeEmUsoException(String.format("O colaborador %s não pode ser excluído pois possui avaliações realizadas como gestor", colaborador.getIdColaborador()));
        }

        colaboradorRepository.delete(colaborador);
    }

    public List<Colaborador> listarColaboradorComCargo(Long idCargo) {
        return colaboradorRepository.findAllByCargo_IdCargo(idCargo);
    }
}
