package com.humanresources.colaborador;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "Colaborador", description = "CRUD de Colaborador")
@RestController
@RequestMapping("/colaboradores")
public class ColaboradorController {

    @Autowired
    private ColaboradorService colaboradorService;

    @GetMapping
    public ResponseEntity listar(Pageable pageable) {
        return ResponseEntity.ok(colaboradorService.listar(pageable));
    }

    @GetMapping("/{idGestor}/gerenciados")
    public ResponseEntity listarColaboradoresGerenciados(@PathVariable Long idGestor) {
        return ResponseEntity.ok(colaboradorService.listarGerenciadosPor(idGestor));
    }

    @GetMapping("/{idColaborador}")
    public ResponseEntity buscar(@PathVariable Long idColaborador) {
        return ResponseEntity.ok(colaboradorService.buscarOuFalhar(idColaborador));
    }

    @PostMapping
    public ResponseEntity salvar(//UriComponentsBuilder uriComponentsBuilder,
                                 @RequestBody ColaboradorForm colaboradorForm) {

        return ResponseEntity.ok(colaboradorService.salvar(colaboradorForm));
    }

    @PutMapping("/{idColaborador}")
    public ResponseEntity atualizar(@PathVariable Long idColaborador,
                                    @RequestBody ColaboradorForm colaboradorForm) {

        return ResponseEntity.ok(colaboradorService.atualizar(idColaborador, colaboradorForm));
    }

    //TODO não funcionou pcausa da avaliacao
    @DeleteMapping("/{idColaborador}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long idColaborador) {

        colaboradorService.remover(idColaborador);
    }
}
