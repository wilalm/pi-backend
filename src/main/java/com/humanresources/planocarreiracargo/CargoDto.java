package com.humanresources.planocarreiracargo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CargoDto {

    private Long idCargo;

    private String nome;
    private String descricao;
    private BigDecimal salarioMinRecomendado;
    private BigDecimal salarioMaxRecomendado;
}
