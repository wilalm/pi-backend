package com.humanresources.planocarreiracargo;

import lombok.Data;

@Data
public class PlanoCarreiraCargoForm {

    private Long idCargo;
    private int sequencia;
}
