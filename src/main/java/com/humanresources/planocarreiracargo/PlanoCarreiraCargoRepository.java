package com.humanresources.planocarreiracargo;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PlanoCarreiraCargoRepository extends JpaRepository<PlanoCarreiraCargo, Long> {

    List<PlanoCarreiraCargo> findByIdPlanoCarreira(Long idPlanoCarreira);
    Optional<PlanoCarreiraCargo> findByIdPlanoCarreiraAndCargoIdCargo(Long idPlanoCarreira, Long idCargo);
    Optional<PlanoCarreiraCargo> findByIdPlanoCarreiraAndSequencia(Long idPlanoCarreira, int sequencia);
}
