package com.humanresources.planocarreiracargo;

import com.humanresources.cargo.Cargo;
import com.humanresources.cargo.CargoService;
import com.humanresources.planocarreira.PlanoCarreira;
import com.humanresources.planocarreira.PlanoCarreiraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class PlanoCarreiraCargoService {

    @Autowired
    private CargoService cargoService;

    @Autowired
    private PlanoCarreiraService planoCarreiraService;

    @Autowired
    private PlanoCarreiraCargoRepository planoCarreiraCargoRepository;

    public List<PlanoCarreiraCargo> listar(Long idPlanoCarreira) {
        List<PlanoCarreiraCargo> planoCarreiraCargo = planoCarreiraCargoRepository.findByIdPlanoCarreira(idPlanoCarreira);

        planoCarreiraCargo.forEach(PlanoCarreiraCargo::completarCargoDto);

        return planoCarreiraCargo;
    }

    public Optional<PlanoCarreiraCargo> buscarPorIdPlanoCarreiraESequencia(Long idPlanoCarreira, int sequencia) {
        return planoCarreiraCargoRepository.findByIdPlanoCarreiraAndSequencia(idPlanoCarreira, sequencia);
    }

    @Transactional
    public void associar(Long idPlanoCarreira, PlanoCarreiraCargoForm planoCarreiraCargoForm) {
        PlanoCarreira planoCarreira = planoCarreiraService.buscarOuFalhar(idPlanoCarreira);
        Cargo cargo = cargoService.buscarOuFalhar(planoCarreiraCargoForm.getIdCargo());

        PlanoCarreiraCargo novoCargo = PlanoCarreiraCargo.builder()
                .idPlanoCarreira(planoCarreira.getIdPlanoCarreira())
                .cargo(cargo)
                .sequencia(planoCarreiraCargoForm.getSequencia())
                .build();

        planoCarreiraCargoRepository.save(novoCargo);
    }

    @Transactional
    public void desassociar(Long idPlanoCarreira, Long idCargo) {
        planoCarreiraService.buscarOuFalhar(idPlanoCarreira);
        cargoService.buscarOuFalhar(idCargo);

        planoCarreiraCargoRepository.findByIdPlanoCarreiraAndCargoIdCargo(idPlanoCarreira, idCargo)
                .ifPresent(planoCarreiraCargo -> planoCarreiraCargoRepository.delete(planoCarreiraCargo));
    }
}
