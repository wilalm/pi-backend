package com.humanresources.usuario;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "Usuário", description = "CRUD de Usuário")
@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping
    public ResponseEntity listar(Pageable pageable) {
        return ResponseEntity.ok(usuarioService.listar(pageable));
    }

    @GetMapping("/{idUsuario}")
    public ResponseEntity buscar(@PathVariable Long idUsuario) {
        return ResponseEntity.ok(usuarioService.buscarOuFalhar(idUsuario));
    }

    @PostMapping
    public ResponseEntity salvar(//UriComponentsBuilder uriComponentsBuilder,
                                 @RequestBody UsuarioForm usuarioForm) {

        return ResponseEntity.ok(usuarioService.salvar(usuarioForm));
    }

    @PutMapping("/{idUsuario}")
    public ResponseEntity atualizar(@PathVariable Long idUsuario,
                                    @RequestBody UsuarioForm usuarioForm) {

        return ResponseEntity.ok(usuarioService.atualizar(idUsuario, usuarioForm));
    }

    @DeleteMapping("/{idUsuario}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long idUsuario) {

        usuarioService.remover(idUsuario);
    }
}
