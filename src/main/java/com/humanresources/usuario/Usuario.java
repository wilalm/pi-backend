package com.humanresources.usuario;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.humanresources.colaborador.Colaborador;
import com.humanresources.grupo.Grupo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUsuario;

    private String email;
    private String senha;

    @CreationTimestamp
    private LocalDateTime dataCadastro;

    @JsonIgnoreProperties({
            "permissoes"
    })
    @ManyToOne
    @JoinColumn(name = "id_grupo")
    private Grupo grupo;

//    @OneToOne
//    @JoinColumn(name = "id_colaborador")
//    @JsonIgnoreProperties({
//            "rg", "cpf", "ctps", "data_nascimento", "salario", "departamento", "cargo", "avaliacoes", "usuario", "eventos"
//    })
//    private Colaborador colaborador;

    @ManyToOne
    @JoinColumn(name = "id_colaborador")
    @JsonIgnoreProperties({
            "rg", "cpf", "ctps", "data_contratacao", "data_nascimento", "salario", "departamento", "cargo", "avaliacoes", "usuario", "eventos"
    })
    private Colaborador colaborador;
}
