package com.humanresources.usuario;

import com.humanresources.colaborador.Colaborador;
import com.humanresources.colaborador.ColaboradorService;
import com.humanresources.exception.exceptions.EntidadeNaoEncontradaException;
import com.humanresources.grupo.Grupo;
import com.humanresources.grupo.GrupoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UsuarioService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private ColaboradorService colaboradorService;

    @Autowired
    private GrupoService grupoService;

    public Page<Usuario> listar(Pageable pageable) {
        return usuarioRepository.findAll(pageable);
    }

    public Usuario buscarOuFalhar(Long idUsuario) {
        return usuarioRepository.findById(idUsuario)
                .orElseThrow(() ->
                        new EntidadeNaoEncontradaException(
                                String.format("Não existe um usuário com id %s", idUsuario)));
    }


    public Usuario salvar(UsuarioForm usuarioForm) {

        Colaborador colaborador = colaboradorService.buscarOuFalhar(usuarioForm.getIdColaborador());
        Grupo grupo = grupoService.buscarOuFalhar(usuarioForm.getIdGrupo());

        Usuario novoUsuario = usuarioForm.transformar();
        novoUsuario.setColaborador(colaborador);
        novoUsuario.setGrupo(grupo);

        return usuarioRepository.save(novoUsuario);
    }


    public Usuario atualizar(Long idFuncionario, UsuarioForm usuarioForm) {
        Usuario usuarioAtual = buscarOuFalhar(idFuncionario);

        if (usuarioForm.getIdGrupo() != null) {
            usuarioForm.setGrupo(grupoService.buscarOuFalhar(usuarioForm.getIdGrupo()));
        }

        modelMapper.map(usuarioForm, usuarioAtual);

        return usuarioRepository.save(usuarioAtual);
    }

    @Transactional
    public void remover(Long idUsuario) {
        Usuario usuario = buscarOuFalhar(idUsuario);
        usuarioRepository.delete(usuario);
    }
}
