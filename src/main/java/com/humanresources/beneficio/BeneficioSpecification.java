package com.humanresources.beneficio;

import org.springframework.data.jpa.domain.Specification;

public class BeneficioSpecification {

    private BeneficioSpecification() {
        throw new IllegalStateException("Utility class");
    }

    public static Specification<Beneficio> comId(Long idBeneficio) {
        return (root, query, builder) ->
                builder.equal(root.get(Beneficio_.ID_BENEFICIO), idBeneficio);
    }
}
