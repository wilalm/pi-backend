package com.humanresources.beneficio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface BeneficioRepository extends
        JpaRepository<Beneficio, Long>,
        JpaSpecificationExecutor<Beneficio> {
}
