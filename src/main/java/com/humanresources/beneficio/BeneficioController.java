package com.humanresources.beneficio;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "Benefícios", description = "CRUD de Benefício")
@RestController
@RequestMapping("/beneficios")
public class BeneficioController {

    @Autowired
    private BeneficioService beneficioService;

    @GetMapping
    public ResponseEntity listar(Pageable pageable) {
        return ResponseEntity.ok(beneficioService.listar(pageable));
    }

    @GetMapping("/{idBeneficio}")
    public ResponseEntity buscar(@PathVariable Long idBeneficio) {
        return ResponseEntity.ok(beneficioService.buscarOuFalhar(idBeneficio));
    }

    @PostMapping
    public ResponseEntity salvar(//UriComponentsBuilder uriComponentsBuilder,
                                 @RequestBody BeneficioForm beneficioForm) {

        return ResponseEntity.ok(beneficioService.salvar(beneficioForm));
    }

    @PutMapping("/{idBeneficio}")
    public ResponseEntity atualizar(@PathVariable Long idBeneficio,
                                    @RequestBody BeneficioForm beneficioForm) {

        return ResponseEntity.ok(beneficioService.atualizar(idBeneficio, beneficioForm));
    }

    @DeleteMapping("/{idBeneficio}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long idBeneficio) {

        beneficioService.remover(idBeneficio);
    }

}
