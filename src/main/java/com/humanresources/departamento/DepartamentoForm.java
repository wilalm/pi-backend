package com.humanresources.departamento;

import lombok.Data;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

@Data
public class DepartamentoForm {

    @Autowired
    private ModelMapper modelMapper;

    private String nome;
    private String descricao;
    private String localizacao;

    public Departamento transformar() {
        return Departamento.builder()
                .nome(this.nome)
                .descricao(this.descricao)
                .localizacao(this.localizacao)
                .build();
    }
}
