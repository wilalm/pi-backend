package com.humanresources.departamento;

import com.humanresources.exception.exceptions.EntidadeEmUsoException;
import com.humanresources.exception.exceptions.EntidadeNaoEncontradaException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartamentoService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private DepartamentoRepository departamentoRepository;

    public Page<Departamento> listar(Pageable pageable) {
        return departamentoRepository.findAll(pageable);
    }

    public List<Departamento> listar() {
        return departamentoRepository.findAll();
    }

    public Departamento buscarOuFalhar(Long idDepartamento) {
        return departamentoRepository.findById(idDepartamento)
                .orElseThrow(() ->
                        new EntidadeNaoEncontradaException(
                                String.format("Não existe um departamento com id %s", idDepartamento)));
    }


    public Departamento salvar(DepartamentoForm departamentoForm) {

        Departamento novoDepartamento = departamentoForm.transformar();

        return departamentoRepository.save(novoDepartamento);
    }


    public Departamento atualizar(Long idDepartamento, DepartamentoForm departamentoForm) {
        Departamento departamentoAtual = buscarOuFalhar(idDepartamento);

        modelMapper.map(departamentoForm, departamentoAtual);

        return departamentoRepository.save(departamentoAtual);
    }

    public void remover(Long idDepartamento) {
        try {
            Departamento departamento = buscarOuFalhar(idDepartamento);

            departamentoRepository.delete(departamento);

        } catch (DataIntegrityViolationException e) {
            throw new EntidadeEmUsoException(String.format("O departamento %s não pode ser excluído pois possui funcionários vinculados", idDepartamento));
        }

    }
}
