package com.humanresources.planocarreira;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "PlanoCarreira", description = "CRUD de Plano de Carreira")
@RestController
@RequestMapping("/plano_carreira")
public class PlanoCarreiraController {

    @Autowired
    private PlanoCarreiraService planoCarreiraService;

    @GetMapping
    public ResponseEntity listar(Pageable pageable) {
        return ResponseEntity.ok(planoCarreiraService.listar(pageable));
    }

    @GetMapping("/{idPlanoCarreira}")
    public ResponseEntity buscar(@PathVariable Long idPlanoCarreira) {
        return ResponseEntity.ok(planoCarreiraService.buscarOuFalhar(idPlanoCarreira));
    }

    @PostMapping
    public ResponseEntity salvar(//UriComponentsBuilder uriComponentsBuilder,
                                 @RequestBody PlanoCarreiraForm planoCarreiraForm) {

        return ResponseEntity.ok(planoCarreiraService.salvar(planoCarreiraForm));
    }

    @PutMapping("/{idPlanoCarreira}")
    public ResponseEntity atualizar(@PathVariable Long idPlanoCarreira,
                                    @RequestBody PlanoCarreiraForm planoCarreiraForm) {

        return ResponseEntity.ok(planoCarreiraService.atualizar(idPlanoCarreira, planoCarreiraForm));
    }

    @DeleteMapping("/{idPlanoCarreira}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long idPlanoCarreira) {

        planoCarreiraService.remover(idPlanoCarreira);
    }
}
