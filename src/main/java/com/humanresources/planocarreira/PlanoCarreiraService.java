package com.humanresources.planocarreira;

import com.humanresources.exception.exceptions.EntidadeNaoEncontradaException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PlanoCarreiraService {

    @Autowired
    private PlanoCarreiraRepository planoCarreiraRepository;

    @Autowired
    private ModelMapper modelMapper;

    public Page<PlanoCarreira> listar(Pageable pageable) {
        return planoCarreiraRepository.findAll(pageable);
    }

    public PlanoCarreira buscarOuFalhar(Long idPlanoCarreira) {
        return planoCarreiraRepository.findById(idPlanoCarreira)
                .orElseThrow(() ->
                        new EntidadeNaoEncontradaException(
                                String.format("Não existe um plano de carreira com id %s", idPlanoCarreira)));
    }


    public PlanoCarreira salvar(PlanoCarreiraForm planoCarreiraForm) {

        PlanoCarreira novoPlanoCarreira = planoCarreiraForm.transformar();

        return planoCarreiraRepository.save(novoPlanoCarreira);
    }


    public PlanoCarreira atualizar(Long idPlanoCarreira, PlanoCarreiraForm planoCarreiraForm) {
        PlanoCarreira planoCarreiraAtual = buscarOuFalhar(idPlanoCarreira);

        modelMapper.map(planoCarreiraForm, planoCarreiraAtual);

        return planoCarreiraRepository.save(planoCarreiraAtual);
    }

    public void remover(Long idCargo) {
        PlanoCarreira cargo = buscarOuFalhar(idCargo);

        planoCarreiraRepository.delete(cargo);
    }
}
