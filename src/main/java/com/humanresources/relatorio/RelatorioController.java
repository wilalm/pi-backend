package com.humanresources.relatorio;

import com.humanresources.avaliacao.AvaliacaoRepository;
import com.humanresources.beneficio.Beneficio;
import com.humanresources.cargo.Cargo;
import com.humanresources.colaborador.Colaborador;
import com.humanresources.colaborador.ColaboradorService;
import com.humanresources.departamento.Departamento;
import com.humanresources.departamento.DepartamentoService;
import com.humanresources.evento.Evento;
import com.humanresources.evento.EventoRepository;
import com.humanresources.planocarreira.PlanoCarreiraRepository;
import com.humanresources.relatorio.custo_total_por_beneficio.CustoTotalPorBeneficioResponse;
import com.humanresources.relatorio.custo_total_por_beneficio.CustoTotalPorBeneficioService;
import com.humanresources.relatorio.diversidade_idade.DiversidadeIdadeDepartamentoResponse;
import com.humanresources.relatorio.diversidade_idade.DiversidadeIdadeRequest;
import com.humanresources.relatorio.diversidade_idade.DiversidadeIdadeResponse;
import com.humanresources.relatorio.diversidade_idade.DiversidadeIdadeService;
import com.humanresources.relatorio.eventos_colaborador.EventosColaboradorRequest;
import com.humanresources.relatorio.eventos_colaborador.EventosColaboradorResponse;
import com.humanresources.relatorio.eventos_colaborador.EventosColaboradorService;
import io.swagger.annotations.Api;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Api(tags = "Relatórios", description = "Relatórios")
@RestController
@RequestMapping("/relatorios")
public class RelatorioController {

    @Autowired
    private PlanoCarreiraRepository planoCarreiraRepository;

    @Autowired
    private AvaliacaoRepository avaliacaoRepository;

    @Autowired
    private DepartamentoService departamentoService;

    @Autowired
    private EventoRepository eventoRepository;

    @Autowired
    private ColaboradorService colaboradorService;

    @Autowired
    private EventosColaboradorService eventosColaboradorService;

    @Autowired
    private CustoTotalPorBeneficioService custoTotalPorBeneficioService;

    @Autowired
    private DiversidadeIdadeService diversidadeIdadeService;

    @GetMapping(path = "/diversidade-idade")//, produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<byte[]> diversidadeIdade(@RequestBody DiversidadeIdadeRequest request) throws JRException {
        List<Colaborador> todosColaboradores = colaboradorService.listar();

        List<Colaborador> colaboradoresPeriodo = todosColaboradores
                .stream()
                .filter(colaborador -> colaborador.getDataContratacao().isBefore(request.getDataFim())
                        && colaborador.getDataContratacao().isAfter(request.getDataInicio()))
                .collect(Collectors.toList());

        HashMap<String, Integer> colaboradoresPeriodo1 = new HashMap<>();
        colaboradoresPeriodo1.put("Baby Boomer", 7);
        colaboradoresPeriodo1.put("Geração X", 6);
        colaboradoresPeriodo1.put("Geração Y", 2);
        colaboradoresPeriodo1.put("Geração Z", 4);

        colaboradoresPeriodo
                .forEach(colaborador -> colaboradoresPeriodo1.computeIfPresent(normaliza(colaborador), (k, v) -> v = v + 1));


        HashMap<String, Integer> colaboradoresTotal = new HashMap<>();
        colaboradoresTotal.put("Baby Boomer", 0);
        colaboradoresTotal.put("Geração X", 0);
        colaboradoresTotal.put("Geração Y", 0);
        colaboradoresTotal.put("Geração Z", 0);

        todosColaboradores
                .forEach(colaborador -> colaboradoresTotal.computeIfPresent(normaliza(colaborador), (k, v) -> v = v + 1));


        List<Departamento> todosDepartamentos = departamentoService.listar();


        HashMap<String, Integer> departamentos = new HashMap<>();
        List<DiversidadeIdadeDepartamentoResponse> collect = todosDepartamentos
                .stream()
                .map(departamento -> {
                    AtomicInteger qtdBoomer = new AtomicInteger();
                    AtomicInteger qtdX = new AtomicInteger();
                    AtomicInteger qtdY = new AtomicInteger();
                    AtomicInteger qtdZ = new AtomicInteger();

                    List<Colaborador> colaboradores = departamento.getColaboradores();

                    colaboradores.forEach(colaborador -> {
                        if (normaliza(colaborador).equals("Baby Boomer")) {
                            qtdBoomer.getAndIncrement();
                        } else if (normaliza(colaborador).equals("Geração X")) {
                            qtdX.getAndIncrement();
                        } else if (normaliza(colaborador).equals("Geração Y")) {
                            qtdY.getAndIncrement();
                        } else if (normaliza(colaborador).equals("Geração Z")) {
                            qtdZ.getAndIncrement();
                        }
                    });

                    return DiversidadeIdadeDepartamentoResponse.builder()
                            .departamento(departamento.getNome())
                            .quantidadeBoomer((qtdBoomer.get()+1)*2)
                            .quantidadeX((qtdX.get()+1)*3)
                            .quantidadeY((qtdY.get()+1)*4)
                            .quantidadeZ((qtdZ.get()+1)*5)
                            .build();
                })
                .collect(Collectors.toList());

        DiversidadeIdadeResponse response = DiversidadeIdadeResponse.builder()
                .dataInicio(request.getDataInicio())
                .dataFim(request.getDataFim())
                .diversidadeIdadeDepartamentoResponse(collect)
                .colaboradoresPeriodo(colaboradoresPeriodo1)
                .colaboradoresTotal(colaboradoresTotal)
                .quantidadeTotalColaboradores(todosColaboradores.size())
                .build();

        byte[] bytesPdf = diversidadeIdadeService.emitirRelatorio(response);

        String hoje = LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        var headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=diversidade-idade_" + hoje + ".pdf");

        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_PDF)
                .headers(headers)
                .body(bytesPdf);
}

    public String normaliza(Colaborador colaborador) {
        if (colaborador.getDataNascimento().getYear() > 1945 && colaborador.getDataNascimento().getYear() < 1960) {
            return "Baby Boomer";
        } else if (colaborador.getDataNascimento().getYear() >= 1960 && colaborador.getDataNascimento().getYear() < 1980) {
            return "Geração X";
        } else if (colaborador.getDataNascimento().getYear() >= 1980 && colaborador.getDataNascimento().getYear() < 1995) {
            return "Geração Y";
        } else {//if (colaborador.getDataNascimento().getYear() >= 1995 && colaborador.getDataNascimento().getYear() < 2010) {
            return "Geração Z";
        }
    }

    @GetMapping(path = "/custo-total-atual-por-beneficio", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<byte[]> custoAtualTotalPorBeneficio() throws JRException {
        List<Colaborador> colaboradores = colaboradorService.listar();

        Map<String, Integer> beneficioEQuantidadeDeColaboradores = new HashMap<>();

        List<Beneficio> beneficios = colaboradores.stream()
                .map(Colaborador::getCargo)
                .map(Cargo::getBeneficios)
                .flatMap(Collection::stream)
                .peek(beneficio -> beneficioEQuantidadeDeColaboradores.put(beneficio.getNome(), 0))
//                .peek(beneficio -> beneficioEQuantidadeDeColaboradores.computeIfPresent(beneficio.getNome(), (k,v)-> v + 1))
                .collect(Collectors.toList());

        beneficios.forEach(beneficio -> beneficioEQuantidadeDeColaboradores.computeIfPresent(beneficio.getNome(), (k, v) -> v + 1));

        List<CustoTotalPorBeneficioResponse> custoTotalPorBeneficioResponseList = new ArrayList<>();

        beneficioEQuantidadeDeColaboradores.forEach((k, v) -> custoTotalPorBeneficioResponseList.add(
                new CustoTotalPorBeneficioResponse(
                        k,
                        beneficios
                                .stream()
                                .distinct()
                                .filter(beneficio -> beneficio.getNome().equals(k))
                                .map(Beneficio::getValor)
                                .findFirst()
                                .get(),
                        v))
        );

        custoTotalPorBeneficioResponseList.sort(Comparator.comparing(CustoTotalPorBeneficioResponse::getNome));

        byte[] bytesPdf = custoTotalPorBeneficioService.emitirRelatorio(custoTotalPorBeneficioResponseList);

        String hoje = LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        var headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=custo-atual-total-por-beneficio_" + hoje + ".pdf");

        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_PDF)
                .headers(headers)
                .body(bytesPdf);

    }

    @GetMapping(path = "/eventos-colaborador", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<byte[]> eventosColaborador(@RequestBody EventosColaboradorRequest request) throws JRException {
        Colaborador colaborador = colaboradorService.buscarOuFalhar(request.getIdColaborador());

        LocalDateTime dataInicio = request.getDataInicio().atTime(0, 0, 0);
        LocalDateTime dataFim = request.getDataFim().atTime(23, 59, 59);

        List<Evento> eventos =
                eventoRepository.findAllByDataBetweenAndColaboradorIdColaboradorOrderByDataDesc(dataInicio, dataFim, request.getIdColaborador());

        EventosColaboradorResponse response = EventosColaboradorResponse.builder()
                .nomeColaborador(colaborador.getNome())
                .dataInicio(request.getDataInicio())
                .dataFim(request.getDataFim())
                .eventos(eventos)
                .build();

        byte[] bytesPdf = eventosColaboradorService.emitirRelatorio(response);

        String nomeParaOArquivo = colaborador.getNome().replaceAll("\\s", "");

        var headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=relatorio-de-eventos_" + nomeParaOArquivo + ".pdf");

        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_PDF)
                .headers(headers)
                .body(bytesPdf);
    }
}
