package com.humanresources.relatorio.custo_total_por_beneficio;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CustoTotalPorBeneficioResponse {

    private String nome;
    private BigDecimal valorIndividual;
    private Integer quantidadeColaboradores;
    private BigDecimal valorTotal;

    public CustoTotalPorBeneficioResponse(String nome, BigDecimal valorIndividual, Integer quantidadeColaboradores) {
        this.nome = nome;
        this.valorIndividual = valorIndividual;
        this.quantidadeColaboradores = quantidadeColaboradores;
        this.valorTotal = valorIndividual.multiply(BigDecimal.valueOf(quantidadeColaboradores));
    }
}
