package com.humanresources.relatorio.custo_total_por_beneficio;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

@Service
public class PdfCustoTotalPorBeneficio implements CustoTotalPorBeneficioService {

    @Override
    public byte[] emitirRelatorio(List<CustoTotalPorBeneficioResponse> response) throws JRException {
//        try {
        var inputStream = this.getClass().getResourceAsStream(
                "/relatorios/custo-atual-total-por-beneficio.jasper");

        BigDecimal totalGeral = response.stream()
                .map(CustoTotalPorBeneficioResponse::getValorTotal)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);

        var parametros = new HashMap<String, Object>();
        parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
        parametros.put("data", LocalDate.now());
        parametros.put("total_geral", totalGeral);

        var dataSource = new JRBeanCollectionDataSource(response);
        var jasperPrint = JasperFillManager.fillReport(inputStream, parametros, dataSource);

        return JasperExportManager.exportReportToPdf(jasperPrint);
//        } catch (Exception e) {
//            System.out.println("e.getMessage() = " + e.getMessage());
//            System.out.println("entrou");
//            return new byte[0];
//        }
    }
}
