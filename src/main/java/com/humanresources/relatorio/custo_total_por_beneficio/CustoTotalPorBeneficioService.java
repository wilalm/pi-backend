package com.humanresources.relatorio.custo_total_por_beneficio;

import net.sf.jasperreports.engine.JRException;

import java.util.List;

public interface CustoTotalPorBeneficioService {

    byte[] emitirRelatorio(List<CustoTotalPorBeneficioResponse> response) throws JRException;
}
