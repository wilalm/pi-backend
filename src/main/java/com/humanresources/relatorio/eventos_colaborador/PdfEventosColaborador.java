package com.humanresources.relatorio.eventos_colaborador;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Locale;

@Service
public class PdfEventosColaborador implements EventosColaboradorService {

    @Override
    public byte[] emitirRelatorio(EventosColaboradorResponse response) throws JRException {
//        try {
        var inputStream = this.getClass().getResourceAsStream(
                "/relatorios/eventos-colaborador.jasper");

        var parametros = new HashMap<String, Object>();
        parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
        parametros.put("data_inicio", response.getDataInicio());
        parametros.put("data_fim", response.getDataFim());

        var dataSource = new JRBeanCollectionDataSource(response.getEventos());
        var jasperPrint = JasperFillManager.fillReport(inputStream, parametros, dataSource);

        return JasperExportManager.exportReportToPdf(jasperPrint);
//        } catch (Exception e) {
//            System.out.println("e.getMessage() = " + e.getMessage());
//            System.out.println("entrou");
//            return new byte[0];
//        }
    }
}
