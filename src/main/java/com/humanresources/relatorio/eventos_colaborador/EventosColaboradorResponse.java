package com.humanresources.relatorio.eventos_colaborador;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.humanresources.evento.Evento;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class EventosColaboradorResponse {

    @JsonIgnore
    private String nomeColaborador;
    private LocalDate dataInicio;
    private LocalDate dataFim;

    @JsonIgnore
    private List<Evento> eventos;
}
