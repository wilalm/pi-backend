ALTER TABLE `cargo` CHANGE `descricao` `descricao` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `cargo` CHANGE `nome` `nome` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `avaliacao` CHANGE `comentario` `comentario` VARCHAR(700) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `departamento` CHANGE `descricao` `descricao` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;