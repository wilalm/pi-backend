create table departamento (
	id_departamento bigint not null auto_increment,
	nome varchar(60) not null,
	descricao varchar(60) not null,
    localizacao varchar(60) not null,

	primary key (id_departamento)
) engine=InnoDB default charset=utf8 COLLATE=utf8_unicode_ci;


create table cargo (
	id_cargo bigint not null auto_increment,
	nome varchar(30) NOT NULL,
    descricao varchar(60) NOT NULL,
	salario_min decimal(10,2) not null,
	salario_max decimal(10,2) not null,

	primary key (id_cargo)
) engine=InnoDB default charset=utf8 COLLATE=utf8_unicode_ci;


create table plano_carreira (
	id_plano_carreira bigint not null auto_increment,
	nome varchar(60) not null,
	descricao varchar(255) not null,

	primary key (id_plano_carreira)
) engine=InnoDB default charset=utf8 COLLATE=utf8_unicode_ci;


create table cargo_plano_carreira (
    id_cargo_plano_carreira bigint not null auto_increment,
    id_plano_carreira bigint not null,
	id_cargo bigint not null,

	primary key (id_cargo_plano_carreira)
) engine=InnoDB default charset=utf8;


create table beneficio (
  id_beneficio bigint not null auto_increment,
  nome varchar(255) not null,
  descricao varchar(255) default null,
  valor decimal(10,2) default null,
  coparticipacao float(5,2) default null,

  primary key (id_beneficio)
) engine=InnoDB default charset=utf8 COLLATE=utf8_unicode_ci;


create table cargo_beneficio (
    id_cargo_beneficio bigint not null auto_increment,
	id_cargo bigint not null,
	id_beneficio bigint not null,

	primary key (id_cargo_beneficio)
) engine=InnoDB default charset=utf8;


create table colaborador (
	id_colaborador bigint not null auto_increment,
	id_cargo bigint not null,
	id_departamento bigint not null,
	nome varchar(60) not null,
	rg varchar(20) not null,
	cpf varchar(20) not null,
    ctps varchar(20) not null,
    data_nascimento date not null,
    salario decimal(10,2) not null,
    gestor tinyint(1) not null default 0,

	primary key (id_colaborador)
) engine=InnoDB default charset=utf8;


create table avaliacao (
	id_avaliacao bigint not null auto_increment,
	id_colaborador bigint not null,
	id_gestor bigint not null,
	nota_comportamento float(5,2) not null,
	nota_tecnica float(5,2) not null,
	nota_trabalho_em_equipe float(5,2) not null,
	comentario varchar(255) default null,
	data datetime not null,

	primary key (id_avaliacao)
) engine=InnoDB default charset=utf8;


create table evento (
	id_evento bigint not null auto_increment,
	id_colaborador bigint not null,
	tipo_evento enum('CONTRATACAO','ALTERACAO_SALARIO','ALTERACAO_CARGO','ALTERACAO_DEPARTAMENTO','INCLUSAO_AVALIACAO','INCLUSAO_BENEFICIO','ALTERACAO_BENEFICIO','REMOCAO_BENEFICIO') collate utf8_unicode_ci not null,
	descricao varchar(255) not null,
	data date not null,

	primary key (id_evento)
) engine=InnoDB default charset=utf8;


create table grupo (
	id_grupo bigint not null auto_increment,
	nome varchar(60) not null,

	primary key (id_grupo)
) engine=InnoDB default charset=utf8;


create table permissao (
	id_permissao bigint not null auto_increment,
	nome varchar(60) not null,
	descricao varchar(100) not null,

	primary key (id_permissao)
) engine=InnoDB default charset=utf8;


create table grupo_permissao (
    id_grupo_permissao bigint not null auto_increment,
	id_grupo bigint not null,
	id_permissao bigint not null,

	primary key (id_grupo_permissao)
) engine=InnoDB default charset=utf8;


create table usuario (
	id_usuario bigint not null auto_increment,
	id_colaborador bigint not null,
	id_grupo bigint not null,
	email varchar(255) not null,
	senha varchar(255) not null,
	data_cadastro datetime not null,

	primary key (id_usuario)
) engine=InnoDB default charset=utf8;

alter table usuario add constraint fk_usuario_colaborador foreign key (id_colaborador) references colaborador (id_colaborador);
alter table usuario add constraint fk_usuario_grupo foreign key (id_grupo) references grupo (id_grupo);

alter table cargo_plano_carreira add constraint fk_cargo_plano_carreira_cargo foreign key (id_cargo) references cargo (id_cargo);
alter table cargo_plano_carreira add constraint fk_cargo_plano_carreira_plano_carreira foreign key (id_plano_carreira) references plano_carreira (id_plano_carreira);

alter table cargo_beneficio add constraint fk_cargo_beneficio_cargo foreign key (id_cargo) references cargo (id_cargo);
alter table cargo_beneficio add constraint fk_cargo_beneficio_beneficio foreign key (id_beneficio) references beneficio (id_beneficio);

alter table colaborador add constraint fk_colaborador_cargo foreign key (id_cargo) references cargo (id_cargo);
alter table colaborador add constraint fk_colaborador_departamento foreign key (id_departamento) references departamento (id_departamento);

alter table avaliacao add constraint fk_avaliacao_colaborador_id_colaborador foreign key (id_colaborador) references colaborador (id_colaborador);
alter table avaliacao add constraint fk_avaliacao_colaborador_id_gestor foreign key (id_gestor) references colaborador (id_colaborador);

alter table evento add constraint fk_evento_colaborador foreign key (id_colaborador) references colaborador (id_colaborador);

alter table grupo_permissao add constraint fk_grupo_permissao_grupo foreign key (id_grupo) references grupo (id_grupo);
alter table grupo_permissao add constraint fk_grupo_permissao_permissao foreign key (id_permissao) references permissao (id_permissao);