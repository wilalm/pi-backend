set foreign_key_checks = 0;

delete from grupo_permissao;
delete from permissao;
delete from grupo;
delete from usuario;
delete from colaborador;
delete from cargo;
delete from avaliacao;
delete from evento;
delete from departamento;
delete from plano_carreira_cargo;
delete from cargo_beneficio;
delete from plano_carreira;
delete from beneficio;

set foreign_key_checks = 1;

alter table grupo_permissao auto_increment = 1;
alter table permissao auto_increment = 1;
alter table grupo auto_increment = 1;
alter table usuario auto_increment = 1;
alter table colaborador auto_increment = 1;
alter table cargo auto_increment = 1;
alter table avaliacao auto_increment = 1;
alter table evento auto_increment = 1;
alter table departamento auto_increment = 1;
alter table plano_carreira_cargo auto_increment = 1;
alter table cargo_beneficio auto_increment = 1;
alter table plano_carreira auto_increment = 1;
alter table beneficio auto_increment = 1;

insert into cargo values (1, 'Desenvolvedor Jr', 'Desenvolvedor de Software', 2000.00, 3500.00);
insert into cargo values (2, 'Desenvolvedor Pl', 'Desenvolvedor de Software', 3501.00, 4500.00);
insert into cargo values (3, 'Desenvolvedor Sr', 'Desenvolvedor de Software', 4501.00, 9000.00);
insert into cargo values (4, 'Tech Lead de Desenvolvimento', 'Resposável por uma equipe de desenvolvedores de software', 9001.00, 15000.00);
insert into cargo values (5, 'Gerente de Desenvolvimento', 'Resposável por um time de desenvolvedores software', 15001.00, 30000.00);

insert into cargo values (6, 'Analista de Testes (QA) Jr', 'Testador de software', 2000.00, 3500.00);
insert into cargo values (7, 'Analista de Testes (QA) Pl', 'Testador de software', 3501.00, 4500.00);
insert into cargo values (8, 'Analista de Testes (QA) Sr', 'Testador de software', 4501.00, 9000.00);
insert into cargo values (9, 'Tech Lead de Testes (QA)', 'Resposável por uma equipe de testadores de software', 9001.00, 15000.00);
insert into cargo values (10, 'Gerente de Testes (QA)', 'Resposável por um time de testadores de software', 15001.00, 30000.00);

insert into cargo values (11, 'Analista de RH Jr', 'Analista de Recursos Humanos Júnior', 2000.00, 3500.00);
insert into cargo values (12, 'Analista de RH Pl', 'Analista de Recursos Humanos Pleno', 3501.00, 4500.00);
insert into cargo values (13, 'Analista de RH Sr', 'Analista de Recursos Humanos Senior', 4501.00, 9000.00);
insert into cargo values (14, 'Gerente de RH', 'Gerente de Recursos Humanos', 9001.00, 15000.00);
insert into cargo values (15, 'Diretor(a) de RH', 'Diretor(a) de Recursos Humanos', 15001.00, 30000.00);

insert into cargo values (16, 'Analista de Infra Jr', 'Analista de Infraestrutura Júnior', 2000.00, 3500.00);
insert into cargo values (17, 'Analista de Infra Pl', 'Analista de Infraestrutura Pleno', 3501.00, 4500.00);
insert into cargo values (18, 'Analista de Infra Sr', 'Analista de Infraestrutura Senior', 4501.00, 9000.00);
insert into cargo values (19, 'Gerente de Infra', 'Gerente de Infraestrutura', 9001.00, 15000.00);
insert into cargo values (20, 'Diretor(a) de Infra', 'Diretor(a) de Infraestrutura', 15001.00, 30000.00);

insert into cargo values (21, 'Analista de Suporte Jr', 'Analista de Suporte Técnico (Service Desk) Júnior', 2000.00, 3500.00);
insert into cargo values (22, 'Analista de Suporte Pl', 'Analista de Suporte Técnico (Service Desk) Pleno', 3501.00, 4500.00);
insert into cargo values (23, 'Analista de Suporte Sr', 'Analista de Suporte Técnico (Service Desk) Senior', 4501.00, 9000.00);
insert into cargo values (24, 'Gerente de Suporte', 'Gerente de Suporte Técnico (Service Desk)', 9001.00, 15000.00);
insert into cargo values (25, 'Diretor(a) de Suporte', 'Diretor(a) de Suporte Técnico (Service Desk)', 15001.00, 30000.00);

insert into departamento values (1, 'Desenvolvimento', 'Ambiente para desenvolvedores de software', 'Prédio 1, sala 1');
insert into departamento values (2, 'QA', 'Ambiente para testadores de software', 'Prédio 1, sala 2');
insert into departamento values (3, 'RH', 'Ambiente para profissionais de Recursos Humanos (RH)', 'Prédio 1, sala 3');
insert into departamento values (4, 'Infra', 'Ambiente para profissionais de Infraestrutura', 'Prédio 1, sala 4');
insert into departamento values (5, 'Suporte', 'Ambiente para profissionais de Suporte Técnico', 'Prédio 1, sala 5');


insert into colaborador values (1, 1, 1, 'João Pereira da Silva', '495071687', '70922074011', '59466753888', '1995-12-01', '2020-01-02', 1700.33, 4);
insert into colaborador values (2, 1, 1, 'Ignácio José', '495071687', '62902089031', '39961853824', '1980-10-21', '2020-01-02', 2330.33, 4);
insert into colaborador values (3, 1, 1, 'Silvia Andrade', '495071687', '70928426371', '09642753891', '1994-11-28', '2020-01-02', 2100.33, 4);
insert into colaborador values (4, 5, 1, 'Ana Maria Pires', '420762437', '41914315014', '08776159360', '1959-01-01', '2017-02-15', 16000.00, null);

insert into colaborador values (5, 8, 2, 'Martin Joaquim Ribeiro', '486885677', '54858196640', '99642453891', '1980-11-28', '2018-03-02', 2100.33, 7);
insert into colaborador values (6, 9, 2, 'Larissa Tânia Bernardes', '469714128', '88925070367', '29642753891', '1980-11-28', '2015-11-10', 2100.33, 7);
insert into colaborador values (7, 10, 2, 'Edson Henrique Martins', '234556183', '54825810128', '09642753891', '1995-11-28', '2021-01-25', 21000.33, null);

insert into colaborador values (8, 13, 3, 'José Antonio Prado', '479085031', '23065335042', '54930688502', '1982-02-07', '2017-01-02', 7500.37, 9);
insert into colaborador values (9, 15, 3, 'Tereza Antonieta Jardins', '153778441', '13146561042', '02596465706', '1976-02-07', '2020-01-02', 27500.30, null);

insert into colaborador values (10, 17, 4, 'Sebastiana Eduarda Benedita Vieira', '136988246', '77580159562', '09149523891', '1970-11-28', '2019-01-02', 2100.33, 11);
insert into colaborador values (11, 19, 4, 'Cauã Felipe Dias', '191254903', '24312445982', '09457913891', '1970-11-28', '2020-01-02', 2100.33, null);


insert into plano_carreira values (1, 'Desenvolvimento de Software', 'Plano de carreira para quem deseja seguir a carreira em desenvolvimento de software');
insert into plano_carreira values (2, 'Testes de Software', 'Plano de carreira para quem deseja seguir a carreira em testes de software');
insert into plano_carreira values (3, 'Recursos Humanos', 'Plano de carreira para quem deseja seguir a carreira em recursos humanos');
insert into plano_carreira values (4, 'Infraestrutura', 'Plano de carreira para quem deseja seguir a carreira em infraestrutura');
insert into plano_carreira values (5, 'Suporte Técnico', 'Plano de carreira para quem deseja seguir a carreira em suporte técnico');

insert into plano_carreira_cargo values (1, 1, 1, 1);
insert into plano_carreira_cargo values (2, 1, 2, 2);
insert into plano_carreira_cargo values (3, 1, 3, 3);
insert into plano_carreira_cargo values (4, 1, 4, 4);
insert into plano_carreira_cargo values (5, 1, 5, 5);

insert into plano_carreira_cargo values (6, 2, 6, 1);
insert into plano_carreira_cargo values (7, 2, 7, 2);
insert into plano_carreira_cargo values (8, 2, 8, 3);
insert into plano_carreira_cargo values (9, 2, 9, 4);
insert into plano_carreira_cargo values (10, 2, 10, 5);

insert into plano_carreira_cargo values (11, 3, 11, 1);
insert into plano_carreira_cargo values (12, 3, 12, 2);
insert into plano_carreira_cargo values (13, 3, 13, 3);
insert into plano_carreira_cargo values (14, 3, 14, 4);
insert into plano_carreira_cargo values (15, 3, 15, 5);

insert into plano_carreira_cargo values (16, 4, 16, 1);
insert into plano_carreira_cargo values (17, 4, 17, 2);
insert into plano_carreira_cargo values (18, 4, 18, 3);
insert into plano_carreira_cargo values (19, 4, 19, 4);
insert into plano_carreira_cargo values (20, 4, 20, 5);

insert into plano_carreira_cargo values (21, 5, 21, 1);
insert into plano_carreira_cargo values (22, 5, 22, 2);
insert into plano_carreira_cargo values (23, 5, 23, 3);
insert into plano_carreira_cargo values (24, 5, 24, 4);
insert into plano_carreira_cargo values (25, 5, 25, 5);


insert into beneficio values (1, 'Vale Alimentação', 'Benefício para gastar no mercado', 150.00, 0);
insert into beneficio values (2, 'Vale Refeição', 'Benefício para gastar com alimentação diária', 700.00, 0);
insert into beneficio values (3, 'Vale Transporte', 'Benefício para locomoção', 250.00, 6);
insert into beneficio values (4, 'Auxílio Creche', 'Benefício para filhos de colaboradores', 400.00, 0);
insert into beneficio values (5, 'Psicólogo', 'Benefício para que os colaboradores possam passar pelo psicólogo de sua escolha', 400.00, 0);

insert into beneficio values (6, 'Auxílio Alugel de Carro', 'Benefício para gerencia e diretoria', 1000.00, 0);
insert into beneficio values (7, 'Auxílio Combustível', 'Benefício para gerencia e diretoria', 500.00, 0);
insert into beneficio values (8, 'Auxílio Moradia', 'Benefício para gerencia e diretoria', 2000.00, 0);


insert into cargo_beneficio values (1, 1, 1);
insert into cargo_beneficio values (2, 1, 2);
insert into cargo_beneficio values (3, 1, 3);
insert into cargo_beneficio values (4, 1, 4);

insert into cargo_beneficio values (5, 2, 1);
insert into cargo_beneficio values (6, 2, 2);
insert into cargo_beneficio values (7, 2, 3);
insert into cargo_beneficio values (8, 2, 4);

insert into cargo_beneficio values (9, 3, 1);
insert into cargo_beneficio values (10, 3, 2);
insert into cargo_beneficio values (11, 3, 3);
insert into cargo_beneficio values (12, 3, 4);

insert into cargo_beneficio values (13, 4, 1);
insert into cargo_beneficio values (14, 4, 2);
insert into cargo_beneficio values (15, 4, 3);
insert into cargo_beneficio values (16, 4, 4);

insert into cargo_beneficio values (17, 5, 1);
insert into cargo_beneficio values (18, 5, 2);
insert into cargo_beneficio values (19, 5, 3);
insert into cargo_beneficio values (20, 5, 4);

insert into cargo_beneficio values (21, 6, 1);
insert into cargo_beneficio values (22, 6, 2);
insert into cargo_beneficio values (23, 6, 3);
insert into cargo_beneficio values (24, 6, 4);

insert into cargo_beneficio values (25, 7, 1);
insert into cargo_beneficio values (26, 7, 2);
insert into cargo_beneficio values (27, 7, 3);
insert into cargo_beneficio values (28, 7, 4);

insert into cargo_beneficio values (29, 8, 1);
insert into cargo_beneficio values (30, 8, 2);
insert into cargo_beneficio values (31, 8, 3);
insert into cargo_beneficio values (32, 8, 4);

insert into cargo_beneficio values (33, 9, 1);
insert into cargo_beneficio values (34, 9, 2);
insert into cargo_beneficio values (35, 9, 3);
insert into cargo_beneficio values (36, 9, 4);

insert into cargo_beneficio values (37, 10, 1);
insert into cargo_beneficio values (38, 10, 2);
insert into cargo_beneficio values (39, 10, 3);
insert into cargo_beneficio values (40, 10, 4);

insert into cargo_beneficio values (41, 11, 1);
insert into cargo_beneficio values (42, 11, 2);
insert into cargo_beneficio values (43, 11, 3);
insert into cargo_beneficio values (44, 11, 4);

insert into cargo_beneficio values (45, 12, 1);
insert into cargo_beneficio values (46, 12, 2);
insert into cargo_beneficio values (47, 12, 3);
insert into cargo_beneficio values (48, 12, 4);

insert into cargo_beneficio values (49, 13, 1);
insert into cargo_beneficio values (50, 13, 2);
insert into cargo_beneficio values (51, 13, 3);
insert into cargo_beneficio values (52, 13, 4);

insert into cargo_beneficio values (53, 14, 1);
insert into cargo_beneficio values (54, 14, 2);
insert into cargo_beneficio values (55, 14, 3);
insert into cargo_beneficio values (56, 14, 4);

insert into cargo_beneficio values (57, 15, 1);
insert into cargo_beneficio values (58, 15, 2);
insert into cargo_beneficio values (59, 15, 3);
insert into cargo_beneficio values (60, 15, 4);

insert into cargo_beneficio values (111, 1, 5);
insert into cargo_beneficio values (61, 2, 5);
insert into cargo_beneficio values (62, 3, 5);
insert into cargo_beneficio values (63, 4, 5);
insert into cargo_beneficio values (64, 5, 5);
insert into cargo_beneficio values (65, 6, 5);
insert into cargo_beneficio values (66, 7, 5);
insert into cargo_beneficio values (67, 8, 5);
insert into cargo_beneficio values (68, 9, 5);
insert into cargo_beneficio values (69, 10, 5);
insert into cargo_beneficio values (70, 11, 5);
insert into cargo_beneficio values (71, 12, 5);
insert into cargo_beneficio values (72, 13, 5);
insert into cargo_beneficio values (73, 14, 5);
insert into cargo_beneficio values (74, 15, 5);
insert into cargo_beneficio values (75, 16, 5);
insert into cargo_beneficio values (76, 17, 5);
insert into cargo_beneficio values (77, 18, 5);
insert into cargo_beneficio values (78, 19, 5);
insert into cargo_beneficio values (79, 20, 5);
insert into cargo_beneficio values (80, 21, 5);
insert into cargo_beneficio values (81, 22, 5);
insert into cargo_beneficio values (82, 23, 5);
insert into cargo_beneficio values (83, 24, 5);
insert into cargo_beneficio values (84, 25, 5);

insert into cargo_beneficio values (86, 5, 6);
insert into cargo_beneficio values (88, 10, 6);
insert into cargo_beneficio values (89, 14, 6);
insert into cargo_beneficio values (90, 15, 6);
insert into cargo_beneficio values (91, 19, 6);
insert into cargo_beneficio values (92, 20, 6);
insert into cargo_beneficio values (93, 24, 6);
insert into cargo_beneficio values (94, 25, 6);

insert into cargo_beneficio values (95, 5, 7);
insert into cargo_beneficio values (96, 10, 7);
insert into cargo_beneficio values (97, 14, 7);
insert into cargo_beneficio values (98, 15, 7);
insert into cargo_beneficio values (99, 19, 7);
insert into cargo_beneficio values (100, 20, 7);
insert into cargo_beneficio values (101, 24, 7);
insert into cargo_beneficio values (102, 25, 7);

insert into cargo_beneficio values (103, 5, 8);
insert into cargo_beneficio values (104, 10, 8);
insert into cargo_beneficio values (105, 14, 8);
insert into cargo_beneficio values (106, 15, 8);
insert into cargo_beneficio values (107, 19, 8);
insert into cargo_beneficio values (108, 20, 8);
insert into cargo_beneficio values (109, 24, 8);
insert into cargo_beneficio values (110, 25, 8);

insert into grupo values (1, 'RH');
insert into grupo values (2, 'Infraestrutura');
insert into grupo values (3, 'Colaborador Comum');

insert into permissao values (1, 'Gerenciar Cargos', 'Permite ao colaborador avaliar outros colaboradores');
insert into permissao values (2, 'Gerenciar Departamentos', 'Permite ao colaborador alterar salário');
insert into permissao values (3, 'Gerenciar Colaboradores', 'Permite ao colaborador alterar cargo e, consequentemente, salário');
insert into permissao values (4, 'Gerenciar Benefícios', 'Permite ao colaborador editar um benefício');

insert into permissao values (8, 'Gerenciar Planos de Carreira', 'Permite ao colaborador gerenciar planos de carreira');
insert into permissao values (9, 'Promover Colaboradores', 'Permite ao colaborador promover colaboradores por cargo e salário');
insert into permissao values (10, 'Emitir Relatório - Eventos Colaborador', 'Permite ao colaborador emitir o relatório Eventos Colaborador');
insert into permissao values (11, 'Emitir Relatório - Diversidade - Idade', 'Permite ao colaborador emitir o relatório Diversidade - Idade');
insert into permissao values (12, 'Emitir Relatório - Custo Total por Benefício', 'Permite ao colaborador emitir o relatório Custo Total por Benefício');

insert into permissao values (5, 'Gerenciar Usuários', 'Permite ao colaborador conceder benefícios');
insert into permissao values (6, 'Gerenciar Grupos', 'Permite ao colaborador remover benefício');

insert into permissao values (7, 'Visualizar informações próprias e histórico', 'Permite ao colaborador remover benefício');


insert into grupo_permissao values (1, 1, 1);
insert into grupo_permissao values (2, 1, 2);
insert into grupo_permissao values (3, 1, 3);
insert into grupo_permissao values (4, 1, 4);
insert into grupo_permissao values (5, 1, 7);
insert into grupo_permissao values (10, 1, 8);
insert into grupo_permissao values (11, 1, 9);
insert into grupo_permissao values (12, 1, 10);
insert into grupo_permissao values (13, 1, 11);
insert into grupo_permissao values (14, 1, 12);


insert into grupo_permissao values (6, 2, 5);
insert into grupo_permissao values (7, 2, 6);
insert into grupo_permissao values (8, 2, 7);
insert into grupo_permissao values (15, 2, 10);

insert into grupo_permissao values (9, 3, 7);
insert into grupo_permissao values (16, 3, 10);

insert into usuario values (1, 1, 3, 'joao_silva@empresa.com', '123', '2020-01-02');
insert into usuario values (2, 2, 3, 'ignacio_jose@empresa.com', '123', '2020-01-02');
insert into usuario values (3, 3, 3, 'silvinha_andrade@empresa.com', '123', '2020-01-02');
insert into usuario values (4, 4, 3, 'ana_pires@empresa.com', '123', '2019-11-02');
insert into usuario values (5, 5, 3, 'martin_ribeiro@empresa.com', '123', '2018-03-02');
insert into usuario values (6, 6, 3, 'larissa_bernardes@empresa.com', '123', '2015-11-10');
insert into usuario values (7, 7, 3, 'edson_martins@empresa.com', '123', '2021-01-25');

insert into usuario values (8, 8, 1, 'jose_prado@empresa.com', '123', '2017-01-02');
insert into usuario values (9, 9, 1, 'tereza_jardins@empresa.com', '123', '2020-01-02');

insert into usuario values (10, 10, 2, 'sebastiana_vieira@empresa.com', '123', '2019-01-02');
insert into usuario values (11, 11, 2, 'caua_dias@empresa.com', '123', '2020-01-02');


insert into evento values (1, 4, 'CONTRATACAO', 'Contratação efetuada', '2015-02-02 08:00:00');
insert into evento values (2, 4, 'ALTERACAO_CARGO', 'Alteração no cargo, movido de Desenvolvedor Pl para Desenvolvedor Sr com promoção salarial de R$4500,00 para R$6500,00.\nBenefícios perdidos: []\nBenefícios ganhados: []', '2016-02-15 08:00:00');
insert into evento values (3, 4, 'ALTERACAO_CARGO', 'Alteração no cargo, movido de Desenvolvedor Sr para Tech Lead de Desenvolvimento com promoção salarial de R$8000,00 para R$9010,00.\nBenefícios perdidos: []\nBenefícios ganhados: [Auxílio Creche]', '2017-02-15 08:00:00');
insert into evento values (4, 4, 'ALTERACAO_CARGO', 'Alteração no cargo, movido de Tech Lead de Desenvolvimento para Gerente de Desenvolvimento com promoção salarial de R$12750,00 para R$15010,00.\nBenefícios perdidos: []\nBenefícios ganhados: [Auxílio Alugel de Carro, Auxílio Combustível, Auxílio Moradia]', '2019-02-15 08:00:00');

insert into evento values (5, 4, 'ALTERACAO_SALARIO', 'Alteração no salário de R$6500,00 para R$8000,00', '2016-10-15 08:00:00');
insert into evento values (6, 4, 'ALTERACAO_SALARIO', 'Alteração no salário de R$9010,00 para R$12750,00', '2017-11-22 08:00:00');
insert into evento values (7, 4, 'ALTERACAO_SALARIO', 'Alteração no salário de R$15010,00 para R$16000,00', '2020-05-02 08:00:00');

insert into evento values (8, 4, 'INCLUSAO_AVALIACAO', 'Inclusão de nova avaliação', '2017-08-15 10:20:23');
insert into evento values (9, 4, 'INCLUSAO_AVALIACAO', 'Inclusão de nova avaliação', '2016-06-17 15:22:20');
insert into evento values (10, 4, 'INCLUSAO_AVALIACAO', 'Inclusão de nova avaliação', '2017-02-02 12:45:13');
insert into evento values (11, 4, 'INCLUSAO_AVALIACAO', 'Inclusão de nova avaliação', '2018-09-28 09:52:08');

insert into avaliacao values (1, 4, 6, 10.00, 10.00, 10.00, 'Foi um exemplo, lidou bem com todas os desafios propostos, segue crescendo onde é orientada', '2017-08-15 10:20:22');
insert into avaliacao values (2, 4, 7, 10.00, 10.00, 10.00, 'Lidou bem com todos os desafios enfrentados, é possível perceber engajamento tanto dentro quanto fora do trabalho, pois tem se qualificado cada vez mais', '2016-06-17 15:22:20');
insert into avaliacao values (3, 4, 9, 10.00, 10.00, 10.00, 'Atendeu a todas as demandas, superou as expectativas com os clientes', '2017-02-02 12:45:12');
insert into avaliacao values (4, 4, 10, 10.00, 10.00, 10.00, 'Está mandando muito bem, cuidadosa e detalhista em todas as tarefas que realiza', '2018-09-28 09:52:07');
